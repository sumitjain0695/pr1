import sys
import math
def distance(point_1, point_2):
	return ((point_1[0] - point_2[0])**2 + (point_1[1] - point_2[1])**2)**0.5

def minimum_distance(points, start, end):
	if end - start ==1:
		if points[start][1] > points[end][1]:
			points[start], points[end] = points[end], points[start]
		return distance(points[start], points[end])
	if end - start == 2:
		tempPoints = sorted(points[start:end+1], key= lambda x: x[1])
		for i in range(3):
			points[start+i] = tempPoints[i]
		return min(distance(points[start], points[start+1]), distance(points[start], points[end]), distance(points[end], points[start+1]))
	mid = int((start + end)/2)
	print('mid is',mid)
	dl = minimum_distance(points, start, mid+1)
	print('mid is',mid)
	dr = minimum_distance(points, mid+1, end)
	dmin = min(dl, dr)
	print('dmin is',dmin)
	line = points[mid+1][0]
	print('1st line is',line)
	left = start
	print('left is',left)
	right = mid + 1
	print('right is',right)
	tempPoints = sorted(points[start:end+1], key= lambda x: x[1])
	for i in range(len(tempPoints)):
		points[start+i] = tempPoints[i]
	print('points after temp',points)	
	line = points[mid+1][0]
	print('2nd line is',line)
	strip = []
	for point in points[start:end+1]:
		if abs(line-point[0])>dmin: continue
		strip.append(point)
	print('strip is',strip)	
	for i in range(len(strip)):
		point = strip[i]
		for j in range(1,4):
			if i+j>=len(strip): break
			print('comparison bw',point,strip[i+j])
			dmin = min(dmin, distance(point,strip[i+j]))
	print('dmin now',dmin)		
	return dmin


if __name__ == '__main__':
	data = list(map(int, input().split()))
	n = data[0]
	x = data[1::2]
	y = data[2::2]
	points = [[x[i], y[i]] for i in range(len(x))]
	points.sort()
	print('input points are',points)
	print("{0:.9f}".format(minimum_distance(points, 0, len(points)-1)))
