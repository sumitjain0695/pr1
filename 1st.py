for _ in range(int(input())):
    n,m,k=map(int,input().split())
    lx=[]
    ly=[]
    for i in range(k):
        a,b=map(int,input().split())
        lx.append((a,b))
        ly.append((b,a))
    lx=sorted(lx)
    ly=sorted(ly)
    #print('lx is ',lx)
    #print('ly is ',ly)
    cx=0
    cy=0
    for i in range(1,k):
        if lx[i][0]==lx[i-1][0]:
            if lx[i-1][1]==lx[i][1]-1:
                #print('increasing cx for',lx[i-1],lx[i])
                cx+=1
    for i in range(1,k):
        if ly[i][0]==ly[i-1][0]:
            if ly[i-1][1]==ly[i][1]-1:
                #print('increasing cy for',ly[i-1],ly[i])
                cy+=1
    #print('ans is')
    print(k*4-2*(cx+cy))
                
            
        
        
    
    
